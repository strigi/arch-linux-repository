# Arch Linux Repository

Arch Linux is great. Best GNU/Linux distribution I've worked with. Its package
manager `pacman` is also great.

Sometimes though, a program hasn't made it into the official package
repositories yet. In this case they do provide AUR, which contains prewritten
scripts that you can run (using a helper too such as `yay`) to build them
yourself. Unfortunately, for a large program, that takes a very long time and
requires installing additional build dependencies you don't need to just
run the program. So this project is a little experiment to see if I can host
my previously built packages, which took hours and hours to complete in a
package repository of my own, so the next time I (or someone else) needs the
program, the build process from AUR can be skipped and the program can be
installed quickly like any other package.

